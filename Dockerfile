FROM openjdk:11-jre-slim
VOLUME /tmp
EXPOSE 8080
ADD /target/spring-boot-0.0.1.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
